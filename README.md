# vue2-html-template

vue2+html 开发集成项目基础模板

#### 使用方式

yarn / npm install

yarn dev / npm run dev

---
js
- fetch
```
import http from './utils/fetch.js'

http.post(url, data).then()
```

- format
```
import format from './utils/format.js'

format.number()
```

- scroll-to
```
import scrollTo from './utils/scroll-to.js'

scrollTo(0, 800) // 滚动条移动（位置， 时间）
```
---
css
```
// 重置页面默认样式
<link rel="stylesheet" href="./css/reset.css">
// flex样式封装
<link rel="stylesheet" href="./css/common.css">
```