import http from './utils/fetch.js';
import format from './utils/format.js'
const {
    log
} = console;

let app = new Vue({
    el: "#app",
    data() {
        return {
            total: 20,
            listQuery: {
                page: 1,
                limit: 10,
            },
        }
    },
    watch: {},
    mounted() {
        log(format.number(1000234516))
        this.getList()
    },
    methods: {
        async getList() {
            let {answer} = await http.get('https://yesno.wtf/api')
            log(answer)
        }
    }
});